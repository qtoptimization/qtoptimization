%modules = ( # path to module name map
    "QtOptimization" => "$basedir/src/optimization",
);

%moduleheaders = ( # restrict the module headers to those found in relative path
);

%classnames = (
    "qtoptimizationversion.h" => "QtOptimizationVersion",
    "qtoptimizationglobal.h"  => "QtOptimizationGlobal",
    "qtoptimizationnamespace.h"  => "QtOptimizationNamespace",
);

%modulepris = (
    "QtOptimization" => "$basedir/modules/qtoptimization.pri",
);

%dependencies = (
    "qtbase" => "refs/heads/stable",
);
