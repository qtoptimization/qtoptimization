QT.optimization.VERSION = 5.1.0
QT.optimization.MAJOR_VERSION = 5
QT.optimization.MINOR_VERSION = 1
QT.optimization.PATCH_VERSION = 0

QT.optimization.name = QtOptimization
QT.optimization.bins = $$QT_MODULE_BIN_BASE
QT.optimization.includes = $$QT_MODULE_INCLUDE_BASE $$QT_MODULE_INCLUDE_BASE/$$QT.optimization.name
QT.optimization.private_includes = $$QT_MODULE_INCLUDE_BASE/$$QT.optimization.name/$$QT.optimization.VERSION
QT.optimization.sources = $$QT_MODULE_BASE/src/optimization
QT.optimization.libs = $$QT_MODULE_LIB_BASE
QT.optimization.plugins = $$QT_MODULE_PLUGIN_BASE
QT.optimization.imports = $$QT_MODULE_IMPORT_BASE
QT.optimization.depends = core

QT.optimization.DEFINES = QT_OPTIMIZATION_LIB
QT_CONFIG += optimization
