/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qpolynomialmutationoperator.h"
#include "qpolynomialmutationoperator_p.h"

#include "QtOptimization/QNsga2"
#include "QtOptimization/QIndividual"

QPolynomialMutationOperatorPrivate::QPolynomialMutationOperatorPrivate()
{
}

QPolynomialMutationOperator::QPolynomialMutationOperator(qreal mutationProbability, qreal distributionIndex) :
    QMutationOperator (*new QPolynomialMutationOperatorPrivate, mutationProbability)
{
    Q_D(QPolynomialMutationOperator);
    d->distributionIndex = distributionIndex;
}

void QPolynomialMutationOperator::mutate(QIndividual *i) const
{
    Q_D(const QPolynomialMutationOperator);

    int v = 0;
    foreach (const QVariable &variable, i->nsga2()->variables()) {
        Q_ASSERT(variable.type() == QtOptimization::VariableReal);
        qreal p = (double) qrand() / RAND_MAX;
        if (p <= d->mutationProbability) {
            qreal y = i->realVariableValue(v);
            qreal yl = variable.lowerBound();
            qreal yu = variable.upperBound();
            qreal delta1 = (y-yl)/(yu-yl);
            qreal delta2 = (yu-y)/(yu-yl);
            qreal u = (double)qrand()/RAND_MAX;
            qreal mutPow = 1.0/(d->distributionIndex+1.0);
            qreal deltaq, xy, val;
            if (u <= 0.5) {
                xy = 1.0-delta1;
                val = 2.0*u+(1.0-2.0*u)*(pow(xy,(d->distributionIndex+1.0)));
                deltaq = pow(val, mutPow)-1.0;;
            }
            else {
                xy = 1.0-delta2;
                val = 2.0*(1.0-u)+2.0*(u-0.5)*(pow(xy,(d->distributionIndex+1.0)));
                deltaq = 1.0-(pow(val, mutPow));
            }
            y = y + deltaq*(yu-yl);
            if (y<yl) y = yl;
            if (y>yu) y = yu;
            i->setRealVariableValue(v, y);
        }
        ++v;
    }
}

