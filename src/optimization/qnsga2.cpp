/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qnsga2.h"
#include "qnsga2_p.h"

#include "QtOptimization/QIndividual"
#include "QtOptimization/QObjectiveFunction"
#include "QtOptimization/QSelectionOperator"
#include "QtOptimization/QCrossoverOperator"
#include "QtOptimization/QMutationOperator"

#include <QtCore/QSet>
#include <QtCore/QTime>

QT_BEGIN_NAMESPACE

QObjectiveFunction *QNsga2::currentObjectiveFunction = 0;

QNsga2Private::QNsga2Private()
{
}

/*!
    \class QNsga2

    \inmodule QtOptimization

    \brief The main NSGA2 implementation class
 */

QNsga2::QNsga2(QList<QVariable> variables, int maxGenerations, qreal crossoverProbability, qreal mutationProbability) :
    d_ptr (new QNsga2Private)
{
    d_ptr->q_ptr = this;
    qsrand((uint)QTime::currentTime().msec());
    setVariables(variables);
    setMaxGenerations(maxGenerations);
    setCrossoverProbability(crossoverProbability);
    setMutationProbability(mutationProbability);
}

QNsga2::~QNsga2()
{
    delete d_ptr;
}

void QNsga2::setVariables(QList<QVariable> variables)
{
    Q_D(QNsga2);
    d->variables = variables;
}

QList<QVariable> QNsga2::variables() const
{
    Q_D(const QNsga2);
    return d->variables;
}

void QNsga2::setObjectiveFunctions(QList<QObjectiveFunction *> objectiveFunctions)
{
    Q_D(QNsga2);
    d->objectiveFunctions = objectiveFunctions;
}

QList<QObjectiveFunction *> QNsga2::objectiveFunctions() const
{
    Q_D(const QNsga2);
    return d->objectiveFunctions;
}

int QNsga2::populationSize() const
{
    Q_D(const QNsga2);
    return d->population.size();
}

void QNsga2::setMaxGenerations(int maxGenerations)
{
    Q_D(QNsga2);
    d->maxGenerations = maxGenerations;
}

int QNsga2::maxGenerations() const
{
    Q_D(const QNsga2);
    return d->maxGenerations;
}

void QNsga2::setCrossoverProbability(qreal crossoverProbability)
{
    Q_D(QNsga2);
    d->crossoverProbability = crossoverProbability;
}

qreal QNsga2::crossoverProbability() const
{
    Q_D(const QNsga2);
    return d->crossoverProbability;
}

void QNsga2::setMutationProbability(qreal mutationProbability)
{
    Q_D(QNsga2);
    d->mutationProbability = mutationProbability;
}

qreal QNsga2::mutationProbability() const
{
    Q_D(const QNsga2);
    return d->mutationProbability;
}

void QNsga2::setPopulation(QList<QIndividual *> population)
{
    Q_D(QNsga2);
    qDeleteAll(d->population);
    d->population = population;
}

QList<QIndividual *> QNsga2::population() const
{
    Q_D(const QNsga2);
    return d->population;
}

void QNsga2::setSelectionOperator(QSelectionOperator *selectionOperator)
{
    Q_D(QNsga2);
    d->selectionOperator = selectionOperator;
}

void QNsga2::setCrossoverOperator(QCrossoverOperator *crossoverOperator)
{
    Q_D(QNsga2);
    d->crossoverOperator = crossoverOperator;
}

void QNsga2::setMutationOperator(QMutationOperator *mutationOperator)
{
    Q_D(QNsga2);
    d->mutationOperator = mutationOperator;
}

QList< QList<QIndividual *> > QNsga2::nonDominatedSortedPopulation() const
{
    Q_D(const QNsga2);
    return d->nonDominatedSortedPopulation;
}

void QNsga2::createRandomPopulation(int size)
{
    Q_D(QNsga2);
    qDeleteAll(d->population);
    d->population.clear();
    appendRandomPopulation(size);
    fastNonDominatedSort();
}

void QNsga2::appendRandomPopulation(int size)
{
    Q_D(QNsga2);
    qreal lowerBound;
    for (int i = 0; i < size; ++i) {
        QIndividual *individual = new QIndividual(this);
        int v = 0;
        foreach (const QVariable &variable, d->variables) {
            lowerBound = variable.lowerBound();
            qreal randomNumber = lowerBound + (variable.upperBound()-lowerBound)*((double)qrand()/RAND_MAX);
            if (variable.type() == QtOptimization::VariableInteger)
                randomNumber = qRound(randomNumber);
            if (variable.type() == QtOptimization::VariableReal)
                individual->setRealVariableValue(v, randomNumber);
            else
                individual->setBinaryVariableValue(v, QString::number((qlonglong) randomNumber, 2));
            ++v;
        }
        d->population.append(individual);
    }
}

void QNsga2::appendNewPopulation()
{
    Q_D(QNsga2);
    QList<QIndividual *> offSpringPopulation;
    int populationSize = d->population.size();
    int *a1 = new int[populationSize];
    int *a2 = new int[populationSize];

    for (int i = 0; i < populationSize; ++i)
        a1[i] = a2[i] = i;
    for (int i = 0; i < populationSize; ++i) {
        int rand = qrand() % populationSize;
        int temp = a1[rand];
        a1[rand] = a1[i];
        a1[i] = temp;
        rand = qrand() % populationSize;
        temp = a2[rand];
        a2[rand] = a2[i];
        a2[i] = temp;
    }

    QIndividual *p1;
    QIndividual *p2;
    QPair<QIndividual *, QIndividual *> offSpringPair;
    for (int i = 0; i < populationSize; i += 4) {
        p1 = d->selectionOperator->select(d->population.at(a1[i]), d->population.at(a1[i+1]));
        p2 = d->selectionOperator->select(d->population.at(a1[i+2]), d->population.at(a1[i+3]));
        offSpringPair = d->crossoverOperator->crossover(p1, p2);
        d->mutationOperator->mutate(offSpringPair.first);
        d->mutationOperator->mutate(offSpringPair.second);
        offSpringPopulation.append(offSpringPair.first);
        offSpringPopulation.append(offSpringPair.second);
        p1 = d->selectionOperator->select(d->population.at(a2[i]), d->population.at(a2[i+1]));
        p2 = d->selectionOperator->select(d->population.at(a2[i+2]), d->population.at(a2[i+3]));
        offSpringPair = d->crossoverOperator->crossover(p1, p2);
        d->mutationOperator->mutate(offSpringPair.first);
        d->mutationOperator->mutate(offSpringPair.second);
        offSpringPopulation.append(offSpringPair.first);
        offSpringPopulation.append(offSpringPair.second);
    }
    delete [] a1;
    delete [] a2;
    foreach (QIndividual *individual, offSpringPopulation)
        d->population.append(individual);
}

bool crowdingDistanceGreaterThan(QIndividual *p1, QIndividual *p2)
{
    return p1->crowdingDistance() > p2->crowdingDistance();
}

void QNsga2::reducePopulation(int size)
{
    Q_D(QNsga2);
    fastNonDominatedSort();
    d->population.clear();
    foreach (QList<QIndividual *> front, d->nonDominatedSortedPopulation) {
        assignCrowdingDistance(front);
        if (d->population.size()+front.size() <= size) {
            foreach (QIndividual *individual, front)
                d->population.append(individual);
        }
        else {
            qSort(front.begin(), front.end(), crowdingDistanceGreaterThan);
            quint32 populationSize = d->population.size();
            for (quint32 i = 0; i < size-populationSize; ++i)
                d->population.append(front.at(i));
            break;
        }
    }
    fastNonDominatedSort();
}

bool objectiveLessThan(QIndividual *p1, QIndividual *p2)
{
    return QNsga2::currentObjectiveFunction->evaluateOnIndividual(p1) < QNsga2::currentObjectiveFunction->evaluateOnIndividual(p2);
}

void QNsga2::assignCrowdingDistance(QList<QIndividual *> front)
{
    Q_D(QNsga2);
    foreach (QIndividual *individual, front)
        individual->setCrowdingDistance(0.0);
    int objectiveFunctionSize = d->objectiveFunctions.size();
    foreach (QObjectiveFunction *objectiveFunction, d->objectiveFunctions) {
        QNsga2::currentObjectiveFunction = objectiveFunction;
        qSort(front.begin(), front.end(), objectiveLessThan);
        front.first()->setCrowdingDistance(INFINITY);
        front.last()->setCrowdingDistance(INFINITY);
        qreal delta = objectiveFunction->evaluateOnIndividual(front.last())-objectiveFunction->evaluateOnIndividual(front.first());
        int frontSize = front.size();
        for (int i = 1; i < frontSize-1; ++i)
            if (delta != 0)
                front.at(i)->setCrowdingDistance(front.at(i)->crowdingDistance()+
                                                 (((objectiveFunction->evaluateOnIndividual(front.at(i+1))-
                                                   objectiveFunction->evaluateOnIndividual(front.at(i-1)))/delta))/objectiveFunctionSize);
    }
}

void QNsga2::fastNonDominatedSort()
{
    Q_D(QNsga2);
    d->nonDominatedSortedPopulation.clear();
    QHash< QIndividual *, QSet<QIndividual *> > dominatedBy;
    QHash<QIndividual *, quint32> dominationCounter;
    QList<QIndividual *> front1;
    foreach (QIndividual *p, d->population) {
        QSet<QIndividual *> dominatedByP;
        dominationCounter[p] = 0;
        foreach (QIndividual *q, d->population) {
            QtOptimization::Dominance dominance = p->dominance(q);
            if (dominance == QtOptimization::Dominates)
                dominatedByP.insert(q);
            else if (dominance == QtOptimization::Dominated)
                dominationCounter[p]++;
        }
        dominatedBy[p] = dominatedByP;
        if (dominationCounter[p] == 0) {
            p->setRank(1);
            front1.append(p);
        }
    }
    d->nonDominatedSortedPopulation.append(front1);
    int i = 1;
    while (d->nonDominatedSortedPopulation.size() == i && !d->nonDominatedSortedPopulation.at(i-1).isEmpty()) {
        QList<QIndividual *> nextFront;
        foreach (QIndividual *p, d->nonDominatedSortedPopulation.at(i-1)) {
            foreach (QIndividual *q, dominatedBy[p]) {
                dominationCounter[q]--;
                if (dominationCounter[q] == 0) {
                    q->setRank(i+1);
                    nextFront.append(q);
                }
            }
        }
        ++i;
        if (!nextFront.isEmpty())
            d->nonDominatedSortedPopulation.append(nextFront);
    }
}

QT_END_NAMESPACE
