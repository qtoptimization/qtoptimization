/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef QNSGA2_H
#define QNSGA2_H

#include <QtOptimization/QtOptimizationGlobal>

#include "QtOptimization/QVariable"

QT_BEGIN_HEADER

QT_BEGIN_NAMESPACE

QT_MODULE(QtOptimization)

class QIndividual;
class QMutationOperator;
class QObjectiveFunction;
class QSelectionOperator;
class QCrossoverOperator;

class QNsga2Private;

class Q_OPTIMIZATION_EXPORT QNsga2
{
    Q_DECLARE_PRIVATE(QNsga2)

public:
    explicit QNsga2(QList<QVariable> variables = QList<QVariable>(),
                    int maxGenerations = 250,
                    qreal crossoverProbability = 0.5,
                    qreal mutationProbability = 0.5);
    virtual ~QNsga2();

    void setVariables(QList<QVariable> variables);
    QList<QVariable> variables() const;
    void setObjectiveFunctions(QList<QObjectiveFunction *> objectiveFunctions);
    QList<QObjectiveFunction *> objectiveFunctions() const;
    int populationSize() const;
    void setMaxGenerations(int maxGenerations);
    int maxGenerations() const;
    void setCrossoverProbability(qreal crossoverProbability);
    qreal crossoverProbability() const;
    void setMutationProbability(qreal mutationProbability);
    qreal mutationProbability() const;
    void setPopulation(QList<QIndividual *> population);
    QList<QIndividual *> population() const;

    void setSelectionOperator(QSelectionOperator *selectionOperator);
    void setCrossoverOperator(QCrossoverOperator *crossoverOperator);
    void setMutationOperator(QMutationOperator *mutationOperator);

    void createRandomPopulation(int size);
    void appendRandomPopulation(int size);
    void appendNewPopulation();
    void reducePopulation(int size);
    void fastNonDominatedSort();
    QList<QList<QIndividual *> > nonDominatedSortedPopulation() const;

    static QObjectiveFunction *currentObjectiveFunction;

protected:
    QNsga2Private *const d_ptr;
    void assignCrowdingDistance(QList<QIndividual *> front);
};

QT_END_NAMESPACE

QT_END_HEADER

#endif // QNSGA2_H
