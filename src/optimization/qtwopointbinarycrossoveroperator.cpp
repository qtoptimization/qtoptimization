/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qtwopointbinarycrossoveroperator.h"
#include "qtwopointbinarycrossoveroperator_p.h"

#include "QtOptimization/QIndividual"
#include "QtOptimization/QNsga2"

#include <QtCore/QCharRef>

QTwoPointBinaryCrossoverOperatorPrivate::QTwoPointBinaryCrossoverOperatorPrivate()
{
}

QTwoPointBinaryCrossoverOperator::QTwoPointBinaryCrossoverOperator(qreal crossoverProbability) :
    QCrossoverOperator (*new QTwoPointBinaryCrossoverOperatorPrivate, crossoverProbability)
{
}

QPair<QIndividual *, QIndividual *> QTwoPointBinaryCrossoverOperator::crossover(QIndividual *p1, QIndividual *p2) const
{
    Q_D(const QTwoPointBinaryCrossoverOperator);
    QNsga2 *nsga2 = p1->nsga2();
    QIndividual *offspring1 = new QIndividual(nsga2);
    QIndividual *offspring2 = new QIndividual(nsga2);

    int variableSize = nsga2->variables().size();
    for (int v = 0; v < variableSize; ++v) {
        Q_ASSERT(nsga2->variables().at(v).type() == QtOptimization::VariableInteger);

        QString geneC1 = p1->binaryVariableValue(v);
        QString geneC2 = p2->binaryVariableValue(v);

        qreal p = (double)qrand()/RAND_MAX;
        if (p <= d->crossoverProbability) {
            int site1 = qrand() % geneC1.length();
            int site2 = qrand() % geneC1.length();
            if (site1 > site2)
                qSwap(site1, site2);

            for (int j = site1; j < site2; ++j) {
                QChar c = geneC1.at(j);
                geneC1[j] = geneC2.at(j);
                geneC2[j] = c;
            }
        }
        offspring1->setBinaryVariableValue(v, geneC1);
        offspring2->setBinaryVariableValue(v, geneC2);
    }

    return QPair<QIndividual *, QIndividual *>(offspring1, offspring2);
}

