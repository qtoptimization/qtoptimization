load(qt_build_config)

TARGET = QtOptimization
QT = core-private

QMAKE_DOCS = $$PWD/doc/qtoptimization.qdocconf

load(qt_module)

include(optimization.pri)

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS
