/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qindividual.h"
#include "qindividual_p.h"

#include "QtOptimization/QNsga2"
#include "QtOptimization/QObjectiveFunction"

QIndividualPrivate::QIndividualPrivate() :
    rank(0),
    crowdingDistance(0.0)
{
}

QIndividual::QIndividual(QNsga2 *nsga2) :
    d_ptr (new QIndividualPrivate)
{
    d_ptr->q_ptr = this;
    d_ptr->nsga2 = nsga2;
}

QIndividual::~QIndividual()
{
    delete d_ptr;
}

void QIndividual::setRealVariableValue(int index, qreal value)
{
    Q_D(QIndividual);
    if (index < d->realValueForVariable.size())
        d->realValueForVariable[index] = value;
    else
        d->realValueForVariable.insert(index, value);
}

qreal QIndividual::realVariableValue(int index) const
{
    Q_D(const QIndividual);
    Q_ASSERT_X(index < d->realValueForVariable.size(),
               "QIndividual::variableValue",
               QStringLiteral("Variable %1 was not set on this individual !").arg(index).toLatin1());
    return d->realValueForVariable.at(index);
}

void QIndividual::setBinaryVariableValue(int index, QString value)
{
    Q_D(QIndividual);
    int size = (int) log2(d->nsga2->variables().at(index).upperBound()+1) - value.length();
    value.reserve(value.size()+size);
    value.prepend(QString(size, QLatin1Char('0')));
    if (index < d->binaryValueForVariable.size())
        d->binaryValueForVariable[index] = value;
    else
        d->binaryValueForVariable.insert(index, value);
}

QString &QIndividual::binaryVariableValue(int index) const
{
    Q_D(const QIndividual);
    Q_ASSERT_X(index < d->binaryValueForVariable.size(),
               "QIndividual::variableValue",
               QStringLiteral("Variable %1 was not set on this individual !").arg(index).toLatin1());
    return const_cast<QString &>(d->binaryValueForVariable[index]);
}

QtOptimization::Dominance QIndividual::dominance(const QIndividual *other) const
{
    Q_D(const QIndividual);
    bool betterInAtLeastOne = false;
    bool worseInAtLeastOne = false;

    foreach(QObjectiveFunction *objectiveFunction, d->nsga2->objectiveFunctions()) {
        qreal optimizationGoalFactor = objectiveFunction->optimizationGoal() == QtOptimization::OptimizationMaximizes ? -1:1;
        qreal evaluationOnThis = optimizationGoalFactor*objectiveFunction->evaluateOnIndividual(this);
        qreal evaluationOnOther = optimizationGoalFactor*objectiveFunction->evaluateOnIndividual(other);
        if (evaluationOnThis < evaluationOnOther)
            betterInAtLeastOne = true;
        else if (evaluationOnThis > evaluationOnOther)
            worseInAtLeastOne = true;
    }
    return (betterInAtLeastOne && !worseInAtLeastOne) ? QtOptimization::Dominates:
                                                        (!betterInAtLeastOne && worseInAtLeastOne) ?
                                                            QtOptimization::Dominated:QtOptimization::NonDominated;
}

void QIndividual::setRank(int rank)
{
    Q_D(QIndividual);
    d->rank = rank;
}

int QIndividual::rank() const
{
    Q_D(const QIndividual);
    return d->rank;
}

void QIndividual::setCrowdingDistance(qreal crowdingDistance)
{
    Q_D(QIndividual);
    d->crowdingDistance = crowdingDistance;
}

qreal QIndividual::crowdingDistance() const
{
    Q_D(const QIndividual);
    return d->crowdingDistance;
}

QNsga2 *QIndividual::nsga2() const
{
    Q_D(const QIndividual);
    return d->nsga2;
}
