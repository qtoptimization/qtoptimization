/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qvariable.h"
#include "qvariable_p.h"

QVariablePrivate::QVariablePrivate()
{
}

QVariable::QVariable(qreal lowerBound, qreal upperBound, QtOptimization::VariableType type) :
    d_ptr (new QVariablePrivate)
{
    d_ptr->q_ptr = this;
    setLowerBound(lowerBound);
    setUpperBound(upperBound);
    setType(type);
}

QVariable::QVariable(const QVariable &variable) :
    d_ptr (new QVariablePrivate)
{
    d_ptr->q_ptr = this;
    setLowerBound(variable.lowerBound());
    setUpperBound(variable.upperBound());
    setType(variable.type());
}

QVariable::~QVariable()
{
    delete d_ptr;
}

void QVariable::setLowerBound(qreal lowerBound)
{
    Q_D(QVariable);
    d->lowerBound = lowerBound;
}

qreal QVariable::lowerBound() const
{
    Q_D(const QVariable);
    return d->lowerBound;
}

void QVariable::setUpperBound(qreal upperBound)
{
    Q_D(QVariable);
    d->upperBound = upperBound;
}

qreal QVariable::upperBound() const
{
    Q_D(const QVariable);
    return d->upperBound;
}

void QVariable::setType(QtOptimization::VariableType type)
{
    Q_D(QVariable);
    d->type = type;
}

QtOptimization::VariableType QVariable::type() const
{
    Q_D(const QVariable);
    return d->type;
}
