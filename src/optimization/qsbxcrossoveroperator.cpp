/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qsbxcrossoveroperator.h"
#include "qsbxcrossoveroperator_p.h"

#include "QtOptimization/QIndividual"
#include "QtOptimization/QNsga2"

const double QSbxCrossoverOperator::EPS = 1.0e-14;

QSbxCrossoverOperatorPrivate::QSbxCrossoverOperatorPrivate()
{
}

QSbxCrossoverOperator::QSbxCrossoverOperator(qreal crossoverProbability, qreal distributionIndex) :
    QCrossoverOperator (*new QSbxCrossoverOperatorPrivate, crossoverProbability)
{
    Q_D(QSbxCrossoverOperator);
    d->distributionIndex = distributionIndex;
}

QPair<QIndividual *, QIndividual *> QSbxCrossoverOperator::crossover(QIndividual *p1, QIndividual *p2) const
{
    Q_D(const QSbxCrossoverOperator);
    QNsga2 *nsga2 = p1->nsga2();
    QIndividual *offspring1 = new QIndividual(nsga2);
    QIndividual *offspring2 = new QIndividual(nsga2);

    qreal p = (double)qrand()/RAND_MAX;
    if (p <= d->crossoverProbability) {
        int v = 0;
        foreach (const QVariable &variable, nsga2->variables()) {
            Q_ASSERT(variable.type() == QtOptimization::VariableReal);
            qreal p1Value = p1->realVariableValue(v);
            qreal p2Value = p2->realVariableValue(v);
            if ((double)qrand()/RAND_MAX <= 0.5) {
                if (qAbs(p1Value-p2Value) > EPS) {
                    qreal y1, y2;
                    if (p1Value < p2Value) {
                        y1 = p1Value;
                        y2 = p2Value;
                    } else {
                        y1 = p2Value;
                        y2 = p1Value;
                    }
                    qreal yl = variable.lowerBound();
                    qreal yu = variable.upperBound();
                    qreal u = (double)qrand()/RAND_MAX;

                    qreal beta = 1.0+(2.0*(y1-yl)/(y2-y1));
                    qreal alpha = 2.0-pow(beta,-(d->distributionIndex+1.0));

                    qreal betaq;
                    if (u <= (1.0/alpha)) {
                        betaq = pow((u*alpha),(1.0/(d->distributionIndex+1.0)));
                    }
                    else {
                        betaq = pow((1.0/(2.0-u*alpha)),(1.0/(d->distributionIndex+1.0)));
                    }
                    qreal c1 = 0.5*((y1+y2)-betaq*(y2-y1));

                    beta = 1.0+(2.0*(yu-y2)/(y2-y1));
                    alpha = 2.0-pow(beta,-(d->distributionIndex+1.0));

                    if (u <= (1.0/alpha)) {
                        betaq = pow((u*alpha),(1.0/(d->distributionIndex+1.0)));
                    }
                    else {
                        betaq = pow((1.0/(2.0-u*alpha)),(1.0/(d->distributionIndex+1.0)));
                    }
                    qreal c2 = 0.5*((y1+y2)+betaq*(y2-y1));

                    if (c1 < yl) c1 = yl;
                    if (c2 < yl) c2 = yl;
                    if (c1 > yu) c1 = yu;
                    if (c2 > yu) c2 = yu;

                    if ((double)qrand()/RAND_MAX <= 0.5) {
                        offspring1->setRealVariableValue(v, c2);
                        offspring2->setRealVariableValue(v, c1);
                    }
                    else {
                        offspring1->setRealVariableValue(v, c1);
                        offspring2->setRealVariableValue(v, c2);
                    }
                }
                else {
                    offspring1->setRealVariableValue(v, p1Value);
                    offspring2->setRealVariableValue(v, p2Value);
                }
            }
            else {
                offspring1->setRealVariableValue(v, p2Value);
                offspring2->setRealVariableValue(v, p1Value);
            }
            ++v;
        }
    }
    else {
        int variableSize = nsga2->variables().size();
        for (int v = 0; v < variableSize; ++v) {
            offspring1->setRealVariableValue(v, p1->realVariableValue(v));
            offspring2->setRealVariableValue(v, p2->realVariableValue(v));
        }
    }
    return QPair<QIndividual *, QIndividual *>(offspring1, offspring2);
}

