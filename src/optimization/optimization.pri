PUBLIC_HEADERS += \
    qtoptimizationglobal.h \
    qtoptimizationnamespace.h \
    qnsga2.h \
    qindividual.h \
    qvariable.h \
    qobjectivefunction.h \
    qcrossoveroperator.h \
    qsbxcrossoveroperator.h \
    qtwopointbinarycrossoveroperator.h \
    qselectionoperator.h \
    qtournamentselectionoperator.h \
    qmutationoperator.h \
    qpolynomialmutationoperator.h \
    qbitwisemutationoperator.h

PRIVATE_HEADERS += \
    qnsga2_p.h \
    qindividual_p.h \
    qvariable_p.h \
    qobjectivefunction_p.h \
    qcrossoveroperator_p.h \
    qsbxcrossoveroperator_p.h \
    qtwopointbinarycrossoveroperator_p.h \
    qselectionoperator_p.h \
    qtournamentselectionoperator_p.h \
    qmutationoperator_p.h \
    qpolynomialmutationoperator_p.h \
    qbitwisemutationoperator_p.h

SOURCES += \
    qtoptimizationnamespace.cpp \
    qnsga2.cpp \
    qindividual.cpp \
    qvariable.cpp \
    qobjectivefunction.cpp \
    qcrossoveroperator.cpp \
    qsbxcrossoveroperator.cpp \
    qtwopointbinarycrossoveroperator.cpp \
    qselectionoperator.cpp \
    qtournamentselectionoperator.cpp \
    qmutationoperator.cpp \
    qpolynomialmutationoperator.cpp \
    qbitwisemutationoperator.cpp
