TEMPLATE = app
TARGET = fast-nondominated-sort-real
DEPENDPATH += .
INCLUDEPATH += .
LIBS += -lqwt

QT += optimization widgets

# Input
SOURCES += main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
