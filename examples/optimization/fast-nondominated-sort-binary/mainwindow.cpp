/****************************************************************************
**
** Copyright (C) 2013 Sandro S. Andrade <sandroandrade@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtOptimization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_symbol.h>
#include <qwt/qwt_plot_magnifier.h>
#include <qwt/qwt_plot_panner.h>

#include <QtOptimization/QVariable>
#include <QtOptimization/QIndividual>
#include <QtOptimization/QObjectiveFunction>
#include <QtOptimization/QTournamentSelectionOperator>
#include <QtOptimization/QTwoPointBinaryCrossoverOperator>
#include <QtOptimization/QBitwiseMutationOperator>

#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

#include <QtCore/QStringBuilder>

#include <sstream>

class MyObjectiveFunction1 : public QObjectiveFunction
{
public:
    explicit MyObjectiveFunction1(QtOptimization::OptimizationGoal optimizationGoal = QtOptimization::OptimizationMinimizes) :
        QObjectiveFunction(optimizationGoal) {}
    virtual qreal evaluateOnIndividual(const QIndividual *individual) const
    {
        QString gene = individual->binaryVariableValue(0);
        return 1 + gene.count('1');
    }
};

class MyObjectiveFunction2 : public QObjectiveFunction
{
public:
    explicit MyObjectiveFunction2(QtOptimization::OptimizationGoal optimizationGoal = QtOptimization::OptimizationMinimizes) :
        QObjectiveFunction(optimizationGoal) {}
    virtual qreal evaluateOnIndividual(const QIndividual *individual) const
    {
        QString gene;

        qreal v = 0, u;
        for (int i = 1; i < 11; ++i) {
            gene = individual->binaryVariableValue(i);
            u = gene.count('1');
            v += (u < 5) ? 2+u:1;
        }

        QString gene1 = individual->binaryVariableValue(0);
        return v/(1 + gene1.count('1'));
    }
};

bool objectiveFunction1LessThan(const QPointF &p1, const QPointF &p2)
{
    return p1.x() < p2.x();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _plot = new QwtPlot(this);
    _plot->canvas()->setStyleSheet(
                "border: 2px solid Black;"
                "border-radius: 15px;"
                "background-color: qlineargradient( x1: 0, y1: 0, x2: 0, y2: 1,"
                    "stop: 0 LemonChiffon, stop: 1 PaleGoldenrod );"
            );
    _plot->setTitle("Population");

    QwtPlotMagnifier *magnifier = new QwtPlotMagnifier(_plot->canvas());
    magnifier->setMouseButton(Qt::NoButton);

    (void) new QwtPlotPanner(_plot->canvas());

    QWidget *centralWidget = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout(centralWidget);
    QPushButton *button = new QPushButton("Improve");
    connect(button, SIGNAL(clicked()), SLOT(improve()));
    layout->addWidget(button);
    layout->addWidget(_plot);
    setCentralWidget(centralWidget);

    QList<QVariable> variables;
    variables << QVariable(0.0, pow(2, 30)-1, QtOptimization::VariableInteger);
    for (int i = 0; i < 10; ++i)
        variables << QVariable(0.0, 31.0, QtOptimization::VariableInteger);
    _nsga2.setVariables(variables);

    MyObjectiveFunction1 *f1 = new MyObjectiveFunction1;
    //f1->setOptimizationGoal(QtOptimization::OptimizationMaximizes);
    MyObjectiveFunction2 *f2 = new MyObjectiveFunction2;
    //f2->setOptimizationGoal(QtOptimization::OptimizationMaximizes);
    _nsga2.setObjectiveFunctions(QList<QObjectiveFunction *>() << f1 << f2);

    QwtPlotCurve *_optimal = new QwtPlotCurve("Optimal Points");
    QVector<QPointF> optimalPoints;
    for (qreal i = 0; i <= 2; i += 2.0/10)
        optimalPoints << QPointF(i*i, (i-2)*(i-2));
    _optimal->setSamples(optimalPoints);
    _optimal->attach(_plot);

    _nsga2.setSelectionOperator(new QTournamentSelectionOperator);
    _nsga2.setCrossoverOperator(new QTwoPointBinaryCrossoverOperator);
    _nsga2.setMutationOperator(new QBitwiseMutationOperator(0.0125));
    _nsga2.createRandomPopulation(100);

//    for (int i = 0; i < 300; ++i) {
//        _nsga2.appendNewPopulation();
//        _nsga2.reducePopulation(100);
//    }
    improve();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::improve()
{
    foreach(QwtPlotCurve *frontCurve, _frontCurves)
        frontCurve->detach();
    qDeleteAll(_frontCurves);
    _frontCurves.clear();
    _nsga2.appendNewPopulation();
    _nsga2.reducePopulation(100);

    int i = 0;
    foreach (const QList<QIndividual *> &front, _nsga2.nonDominatedSortedPopulation()) {
        QwtPlotCurve *frontCurve = new QwtPlotCurve(QStringLiteral("Front %1").arg(i));
        frontCurve->setSymbol(new QwtSymbol(QwtSymbol::Triangle, QColor("Purple"), QColor(Qt::black), QSize(10, 10)));
        frontCurve->setSymbol(new QwtSymbol(QwtSymbol::Triangle, QColor("Purple"), QColor(Qt::black), QSize(10, 10)));
        QVector<QPointF> samples;
        foreach (QIndividual *individual, front) {
            samples << QPointF(_nsga2.objectiveFunctions().at(0)->evaluateOnIndividual(individual),
                               _nsga2.objectiveFunctions().at(1)->evaluateOnIndividual(individual));
        }
        qSort(samples.begin(), samples.end(), objectiveFunction1LessThan);
        frontCurve->setSamples(samples);
        frontCurve->attach(_plot);
        _frontCurves.append(frontCurve);
        ++i;
    }
    _plot->replot();
}
